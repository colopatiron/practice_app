require "test_helper"

class SessionsHelperTest < ActionView::TestCase
  def setup
    @user = users(:jem)
    remember(@user)
  end

  test "current user returns right user when session is nil" do
    assert_equal @user, current_user
    # user should have been automotically logged in
    assert user_logged_in?
  end

  test "current user returns nil when remember digest is incorrect" do
    # update user with incorrect remember digest
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    assert_nil current_user
    assert_not user_logged_in?
  end
end