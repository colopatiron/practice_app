# frozen_string_literal: true

require "test_helper"

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title, "Practice App"
    assert_equal full_title("Page Title"), "Page Title | Practice App"
  end
end
