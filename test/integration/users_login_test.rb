# frozen_string_literal: true

require "test_helper"

class UsersLoginTest < ActionDispatch::IntegrationTest
  def setup
    # users is YAML test fixture
    @user = users(:jem)
  end

  test "login with invalid information" do
    get login_path
    assert_template "sessions/new"
    post login_path, params: { session: { email:    "",
                                          password: "" } }
    assert_template "sessions/new"
    assert_not flash.empty?
    get root_path
    assert flash.empty?
  end

  test "successful login followed by logout" do
    get login_path
    post login_path, params: { session: { email:    @user.email,
                                          password: "password" } }
    assert user_logged_in?
    assert_redirected_to @user
    follow_redirect!
    assert_template "users/show"
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", user_path(@user)
    get root_path
    assert_select "a[href=?]", signup_path, count: 0

    # Simulate logging out
    delete logout_path
    assert_not user_logged_in?
    assert_redirected_to root_url

    # Simulate logging out in a second window
    delete logout_path
    follow_redirect!
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", user_path(@user), count: 0
    assert_select "a[href=?]", logout_path, count: 0
    assert_select "a[href=?]", signup_path, count: 1
  end

  # NOTE: inside tests, cookies[:symbol] doesn't work, so
  # use cookies["string"] instead

  test "log in with remembering" do
    log_in_as(@user, remember_me: "1")
    assert_equal cookies["remember_token"],
                 assigns(:user).remember_token
  end

  test "log in without remembering" do
    # log in to set the cookie
    log_in_as(@user, remember_me: "1")
    # log in again and check whether cookie is deleted
    log_in_as(@user, remember_me: "0")
    assert_empty cookies["remember_token"]
  end
end
