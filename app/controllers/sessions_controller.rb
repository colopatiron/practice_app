# frozen_string_literal: true

class SessionsController < ApplicationController
  before_action :fetch_user, only: [:create]

  def new; end

  def create
    if @user&.authenticate(params[:session][:password])
      do_login_sequence
    else
      # Use flash.now instead of flash for rendered pages
      flash.now[:danger] = "Invalid email/password combination."
      render "new"
    end
  end

  def destroy
    log_out if logged_in?
    flash[:info] = "You have successfully logged out. See you again soon!"
    redirect_to root_url
  end

  private

    def do_login_sequence
      log_in @user
      # Create a persistent session (or not)
      params[:session][:remember_me] == "1" ? remember(@user) : forget(@user)

      redirect_to @user
    end

    def fetch_user
      @user = User.find_by email: params[:session][:email].downcase
    end
end
