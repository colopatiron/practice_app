# frozen_string_literal: true

# For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
Rails.application.routes.draw do
  get "/login" => "sessions#new"
  post "/login" => "sessions#create"
  delete "/logout" => "sessions#destroy"

  get "/contact" => "static_pages#contact"
  get "/help" => "static_pages#help"
  get "/about" => "static_pages#about"
  root "static_pages#home"

  get "/signup" => "users#new"
  post "/signup" => "users#create"
  resources :users
end
